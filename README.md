<H1> Petunjuk : </H1>

MOHON DIBACA DAN DIPERHATIKAN UNTUK SEMUA CASE DIBAWAH INI

TYPE 1

1. Terdapat API public sangat sederhana 
   Buka https://github.com/HackerNews/API

2. Bahasa yang digunakan menggunakan KOTLIN 
    
3. Tugas kalian adalah buatlah list dan detail list TOP STORY yang harus memenuhi kriteria berikut :
	- Gunakan architectural pattern MVVM
	- Gunakan Retrofit
	- Gunakan Room
	- Gunakan Data Binding
	- Gunakan repository pattern
	
	
4. Tampilan UI dibebaskan

5. Waktu pengerjaan default adalah 6 Jam, selesai atau tidak commit di Git Repository

6. Jika ada yang ingin ditanyakan, silahkan kontak ke nomor 082116150754 / email : samnursa@gmail.com