package com.yp.test.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yp.test.R
import com.yp.test.di.Injection
import com.yp.test.model.TopStory
import com.yp.test.viewmodel.TopStoryViewModel
import com.yp.test.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel:TopStoryViewModel
    private lateinit var adapter: TopStoryAdapter

    var id : String? = "22206116"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupUI()

    }

    private fun setupViewModel() {
        viewModel = ViewModelProvider(this, ViewModelFactory(Injection.providerRepository(), id )).get(TopStoryViewModel::class.java)
        viewModel.topstory.observe(this,renderTopStory)

        viewModel.isViewLoading.observe(this,isViewLoadingObserver)
        viewModel.onMessageError.observe(this,onMessageErrorObserver)
        viewModel.isEmptyList.observe(this,emptyListObserver)
    }
    private fun setupUI(){
        adapter= TopStoryAdapter(viewModel.topstory.value?: emptyList())
        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter= adapter
    }

    //observers
    private val renderTopStory= Observer<List<TopStory>> {
        layoutError.visibility= View.GONE
        layoutEmpty.visibility= View.GONE
        adapter.update(it)
    }

    private val isViewLoadingObserver= Observer<Boolean> {
        val visibility=if(it)View.VISIBLE else View.GONE
        progressBar.visibility= visibility
    }

    private val onMessageErrorObserver= Observer<Any> {
        layoutError.visibility=View.VISIBLE
        layoutEmpty.visibility=View.GONE
        textViewError.text= "Error $it"
    }

    private val emptyListObserver= Observer<Boolean> {
        layoutEmpty.visibility=View.VISIBLE
        layoutError.visibility=View.GONE
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadTopStory()
    }
}
