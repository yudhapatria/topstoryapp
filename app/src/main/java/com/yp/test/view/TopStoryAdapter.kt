package com.yp.test.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yp.test.R
import com.yp.test.model.TopStory

class TopStoryAdapter(private var topScore: List<TopStory>): RecyclerView.Adapter<TopStoryAdapter.MViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopStoryAdapter.MViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_topstory, parent, false)
        return MViewHolder(view)    }

    override fun getItemCount(): Int {
        return topScore.size;
    }

    override fun onBindViewHolder(holder: TopStoryAdapter.MViewHolder, position: Int) {
        val topScorer= topScore[position]

        //render
        holder.textViewName.text= topScorer.getTitle()
//        holder.webView.loadUrl(topScorer.getUrl());



          }
    fun update(data:List<TopStory>){
        topScore= data
        notifyDataSetChanged()
    }
    class MViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        val textViewName: TextView = view.findViewById(R.id.textViewName)
        val webView: WebView = view.findViewById(R.id.webView)
        val buttonLink: Button = view.findViewById(R.id.buttomLink)
    }
}