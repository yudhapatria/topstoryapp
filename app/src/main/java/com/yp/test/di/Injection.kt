package com.yp.test.di

import com.yp.test.model.TopStoryRepository
import com.yp.test.model.TopStorySource

object Injection {
    fun providerRepository():TopStorySource{
        return TopStoryRepository()
    }
}