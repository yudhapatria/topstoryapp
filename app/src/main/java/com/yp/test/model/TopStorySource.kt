package com.yp.test.model

import com.yp.test.data.OperationCallback

interface TopStorySource {
    fun retrieveTopStory(callback: OperationCallback, id : String?)
    fun cancel()
}