package com.yp.test.model

import android.content.ContentValues.TAG
import android.util.Log
import com.yp.test.data.ApiClient
import com.yp.test.data.OperationCallback
import com.yp.test.data.TopStoryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopStoryRepository():TopStorySource{
    private var call: Call<TopStoryResponse>?=null

    override fun retrieveTopStory(callback: OperationCallback, id : String?) {
        call= ApiClient.build()?.topStory(id);
        call?.enqueue(object : Callback<TopStoryResponse>{
            override fun onFailure(call: Call<TopStoryResponse>, t: Throwable) {
                callback.onError(t.message)
            }

            override fun onResponse(
                call: Call<TopStoryResponse>,
                response: Response<TopStoryResponse>
            ) {
                response?.body()?.let {
                    if(response.isSuccessful && (it.isSuccess())){
                        Log.v(TAG, "data ${it.data}")
                        callback.onSuccess(it.data)
                    }else{
                        callback.onError(it.msg)
                    }
                } //To change body of created functions use File | Settings | File Templates.
            }

        })
    }

    override fun cancel() {
        call?.let {
            it.cancel()
        }    }

}