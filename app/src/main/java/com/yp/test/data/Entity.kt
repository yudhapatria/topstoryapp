package com.yp.test.data

import com.yp.test.model.TopStory

data class TopStoryResponse(val status:Int?,val msg:String?,val data:List<TopStory>?){
    fun isSuccess():Boolean= (status==200)
}