package com.yp.test.viewmodel

import android.app.Application
import androidx.annotation.NonNull
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yp.test.data.OperationCallback
import com.yp.test.model.TopStory
import com.yp.test.model.TopStoryRepository
import com.yp.test.model.TopStorySource


class TopStoryViewModel(private val repository: TopStorySource, id : String?): ViewModel(){
    var thisid : String? = id;
    private val _topstory = MutableLiveData<List<TopStory>>().apply { value = emptyList() }
    val topstory: LiveData<List<TopStory>> = _topstory

    private val _isViewLoading= MutableLiveData<Boolean>()
    val isViewLoading:LiveData<Boolean> = _isViewLoading

    private val _onMessageError= MutableLiveData<Any>()
    val onMessageError:LiveData<Any> = _onMessageError

    private val _isEmptyList= MutableLiveData<Boolean>()
    val isEmptyList:LiveData<Boolean> = _isEmptyList

    /*
    If you require that the data be loaded only once, you can consider calling the method
    "loadMuseums()" on constructor. Also, if you rotate the screen, the service will not be called.
     */
    init {
        //loadMuseums()
    }

    fun loadTopStory(){
        _isViewLoading.postValue(true)
        repository.retrieveTopStory(object: OperationCallback {
            override fun onError(obj: Any?) {
                _isViewLoading.postValue(false)
                _onMessageError.postValue( obj)
            }

            override fun onSuccess(obj: Any?) {
                _isViewLoading.postValue(false)

                if(obj!=null && obj is List<*>){
                    if(obj.isEmpty()){
                        _isEmptyList.postValue(true)
                    }else{
                        _topstory.value= obj as List<TopStory>
                    }
                }
            }
        }, thisid)
    }


}