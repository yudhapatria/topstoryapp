package com.yp.test.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yp.test.model.TopStorySource

class ViewModelFactory(private val repository:TopStorySource, id : String?):ViewModelProvider.Factory {
    var idthis = id;
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TopStoryViewModel(repository, idthis) as T
    }
}